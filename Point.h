#ifndef POINT_H
#define POINT_H

#include <GL/freeglut.h>

struct Point {
    float x;
    float y;
    float r;
    float g;
    float b;

    Point() {
        x = 0.0f;
        y = 0.0f;
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
    }

    Point(float x, float y, float r, float g, float b) {
        this->x = x;
        this->y = y;
        this->r = r;
        this->g = g;
        this->b = b;
    }

    void draw() {
        glColor3f(r, g, b);
        
        glBegin(GL_POINTS);
            glVertex2f(x, y);
        glEnd();
    }
};


#endif